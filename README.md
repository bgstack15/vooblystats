# Readme for vooblystats.sh
Voobly website scraper written in POSIX shell! Of course sh is not ideal for parsing html, but I suck at understanding the python3 requests library documentation.

Use the config file to set username and password. I implemented the config file so I can exclude secrets for this scm repo.

This is more of a library of functions to be used for whatever you want, as long as it scrapes the webpages from voobly.com. I use it to list my friends and me, then all the games in our history, and then pull all the stats for those games.

# List available functions
View available functions

    grep -E '^[^_][a-z_]+\(\)' vooblystats.sh

View available functions with brief explanation of how to call.

    grep -A2 -E '^[^_][a-z_]+\(\)' vooblystats.sh

# License
CC-BY-SA 4.0

# Alternatives
* I should have used python implementation https://github.com/happyleavesaoc/python-voobly or at least augmented it.
* Haskell https://github.com/bowswung/voobly-scraper
* C++ https://github.com/kliu31415/aoe2data
* Another python implementation https://github.com/hepter/VooblyAPI-Parse
