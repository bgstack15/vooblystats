OUTDIR=~/dev/vooblystats/files
mkdir -p "${OUTDIR}"

# 18 games per minute, approximately
# cover this sequence 19871439 21184314

# need to fork vooblystats.sh 152 times!

startgameid=19871439
endgameid=21184314
span=8640 # games per 8 hours
x=0
count=152

while test $x -lt $count ;
do
   x=$(( x + 1 ))
   startx="$( printf "%s\n" "${startgameid}+(${x}*${span})" | bc )"
   endx="$(   printf "%s\n" "${startgameid}+(${x}*${span})+${span}-1" | bc )"
   echo "call VS_COOKIEFILE=\$(mktemp) vooblystats.sh ${startx} ${endx} > ${OUTDIR}/gameset-${x}.csv"
   #VS_COOKIEFILE="$(mktemp)" ~/dev/vooblystats/vooblystats.sh ${startx} ${endx} > ${OUTDIR}/gameset-${x}.csv &
done
