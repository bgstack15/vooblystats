#!/bin/sh

# final end value should be near: 21260965
# run4 end value, to stop right before vm4 loop:
# run8 end value: 21281500
test -z "${ENDVALUE}" && ENDVALUE=21281500

# run1 start value: 19899000
# run2 start value: 19903845
# run3 start value: 20365401
# run4 start value: 20367770
# run5 start value: 20369832
# run6 start value: 20450916
# run7 start value: 20497306
# run8 is after vm4 run: 20811621
test -z "${STARTVALUE}" && STARTVALUE=20811621

test -z "${LOGFILE}" && LOGFILE=~/dev/vooblystats/files/run8.csv

cat /dev/null > "${LOGFILE}"
{
   printf "%s %s\n" "START" "$( date -u "+%FT%TZ" )"
   ./vooblystats.py --start "${STARTVALUE}" --end "${ENDVALUE}"
   printf "%s %s\n" "END" "$( date -u "+%FT%TZ" )"
} 2>&1 | tee -a "${LOGFILE}"
